package dataDriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FetchingDataFromApachePoi {

	public static void main(String[] args) throws IOException {
		
		File file = new File("/home/sudharsan/Documents/DataDriven TestData/TestData2.xlsx");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook book = new XSSFWorkbook(fis);
		XSSFSheet sht = book.getSheetAt(0);
		int rows = sht.getPhysicalNumberOfRows();
		for (int i = 0; i < rows; i++) {
			int colmuns = sht.getRow(i).getLastCellNum();
			
			for (int j = 0; j < colmuns; j++) 
			{
				String celldata = sht.getRow(i).getCell(j).getStringCellValue();
				System.out.println(celldata);
			}
			
		}
	}

}
