package dataDriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class DataDrivenRegisterPage {

	public static void main(String[] args) throws BiffException, IOException {
		
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		//driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
		File fil = new File("/home/sudharsan/Documents/DataDriven TestData/Testdata1.xls");
		FileInputStream fis = new FileInputStream(fil);
		Workbook book = Workbook.getWorkbook(fis);
		Sheet st = book.getSheet("Registrationdata");
		
		int rows = st.getRows();
		int columns = st.getColumns();
		
		for (int i = 1; i < rows; i++) {
			String firstname = st.getCell(0, i).getContents();
			String lastname = st.getCell(1, i).getContents();
			String email = st.getCell(2, i).getContents();
			String password = st.getCell(3, i).getContents();
			String confirmpassword = st.getCell(3, i).getContents();
		
		driver.findElement(By.xpath("//a[text()='Register']")).click();
		driver.findElement(By.id("gender-male")).click();
		driver.findElement(By.id("FirstName")).sendKeys(firstname);
		driver.findElement(By.id("LastName")).sendKeys(lastname);
		driver.findElement(By.id("Email")).sendKeys(email);
		driver.findElement(By.id("Password")).sendKeys(password);
		driver.findElement(By.id("ConfirmPassword")).sendKeys(confirmpassword);
		driver.findElement(By.id("register-button")).click();
		driver.findElement(By.linkText("Log out")).click();

	}
	}
}
