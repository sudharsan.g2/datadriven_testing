package dataDriven;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import java.io.FileInputStream;
public class DataTestNg {
	
static WebDriver driver;
	
	@BeforeClass
	  public void launchBrowser() {
		 driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
	  }
	@DataProvider(name = "login")
	public String [][] getData() {
		String loginData[][] = {
				{"davidbilla07@gmail.com","David@07"},	// valid data
				{"manuwarrier56@gmail.com","Manu@123"}	// Invalid data
		};
	return loginData;
		}
	
	@Test(dataProvider = "loginData")
	public void login(String user,String pass) {
		  
		  driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
			WebElement name = driver.findElement(By.id("Email"));
			name.clear();
			name.sendKeys(user);
			
			WebElement word = driver.findElement(By.id("Password"));
			word.clear();
			word.sendKeys(pass);
			
			driver.findElement(By.xpath("//input[@class='button-1 login-button']")).click();
			driver.findElement(By.linkText("Log out")).click();
			
	  }
	
	@AfterClass
	  public void afterClass() {
		  
		 
			driver.quit();
	  }

}
