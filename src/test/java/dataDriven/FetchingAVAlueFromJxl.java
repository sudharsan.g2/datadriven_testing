package dataDriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class FetchingAVAlueFromJxl {

	public static void main(String[] args) throws BiffException, IOException {
		
		File file = new File("/home/sudharsan/Documents/DataDriven TestData/Testdata1.xls");
		FileInputStream fil = new FileInputStream(file);
		Workbook  book = Workbook.getWorkbook(fil);
		Sheet sht = book.getSheet("Sheet1");
		String celldata = sht.getCell(0, 0).getContents();
		System.out.println("Value prescent is : " +celldata);
		
		int rows = sht.getRows();
		int columns = sht.getColumns();
		
		for (int i = 0; i < rows; i++) {
			
			for (int j = 0; j < columns; j++) {
				celldata = sht.getCell(j, i).getContents();
				System.out.println(celldata);
				
			}
			
		}
	}

}
