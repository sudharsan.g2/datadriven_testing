package dataDriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class PassingMultipleDataFromJxl {

	public static void main(String[] args) throws BiffException, IOException {
		

		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		//driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
		File file = new File("/home/sudharsan/Documents/DataDriven TestData/Testdata1.xls");
		FileInputStream fil = new FileInputStream(file);
		Workbook  book = Workbook.getWorkbook(fil);
		Sheet sht = book.getSheet("Sheet1");
		//String celldata = sht.getCell(0, 0).getContents();
		//System.out.println("Value prescent is : " +celldata);
		
		int rows = sht.getRows();
		int columns = sht.getColumns();
		
		for (int i = 1; i < rows; i++) {
			String username = sht.getCell(0,i).getContents();	
			String password = sht.getCell(1,i).getContents();
		
		driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
		driver.findElement(By.id("Email")).sendKeys(username);
		driver.findElement(By.id("Password")).sendKeys(password);
		driver.findElement(By.xpath("//input[@class='button-1 login-button']")).click();
		
		driver.findElement(By.linkText("Log out")).click();

	}

}
}