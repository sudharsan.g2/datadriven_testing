package dataDriven;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Login{

	@Test(dataProvider = "getData", dataProviderClass = DataProviderTest.class)
	public void login(String user,String pass) {
		
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		  driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
			WebElement name = driver.findElement(By.id("Email"));
			//name.clear();
			name.sendKeys(user);
			
			WebElement word = driver.findElement(By.id("Password"));
			//word.clear();
			word.sendKeys(pass);
			
			driver.findElement(By.xpath("//input[@class='button-1 login-button']")).click();
			driver.findElement(By.linkText("Log out")).click();
			driver.quit();
	  }
		
		

	}


