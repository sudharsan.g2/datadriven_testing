package dataDriven;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Duration;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginUsingApachePoi {

	public static void main(String[] args) throws IOException {
		
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		//driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
		File file = new File("/home/sudharsan/Documents/DataDriven TestData/TestData2.xlsx");
		FileInputStream fis = new FileInputStream(file);
		XSSFWorkbook book = new XSSFWorkbook(fis);
		XSSFSheet sht = book.getSheetAt(0);
		int rows = sht.getLastRowNum();
		for (int i = 1; i <= rows; i++) 
		{
			
			String username = sht.getRow(i).getCell(0).getStringCellValue();
			String password = sht.getRow(i).getCell(1).getStringCellValue();
			
			driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
			driver.findElement(By.id("Email")).sendKeys(username);
			driver.findElement(By.id("Password")).sendKeys(password);
			driver.findElement(By.xpath("//input[@class='button-1 login-button']")).click();
			
			driver.findElement(By.linkText("Log out")).click();
		}
	}

}
